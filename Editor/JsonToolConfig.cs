using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace DailyCode.JsonTool
{
    public class JsonToolConfig : ScriptableObject
    {
        public string SaveFilePath;
        public bool AutoCreateOnBuild = false;
        [SerializeField] private List<ScriptableObject> _dataList = new List<ScriptableObject>();
        private static JsonToolConfig config;


        public List<ScriptableObject> DataList => _dataList;

        public static JsonToolConfig Config => config;


        [InitializeOnLoadMethod]
        public static JsonToolConfig LoadOrCreateConfig()
        {
            var getGUID = AssetDatabase.FindAssets("t:JsonToolConfig");
            if (getGUID.Length > 0)
                config = AssetDatabase.LoadAssetAtPath<JsonToolConfig>(AssetDatabase.GUIDToAssetPath(getGUID[0]));

            if (config == null)
            {
                config = ScriptableObject.CreateInstance<JsonToolConfig>();

                System.IO.Directory.CreateDirectory("Assets/Editor/");
                AssetDatabase.CreateAsset(config, "Assets/Editor/JsonToolConfig.asset");
            }

            Selection.activeObject = config;
            return config;
        }
    }
}