﻿using UnityEditor.Build;
using UnityEditor.Build.Reporting;

namespace DailyCode.JsonTool
{
    namespace JsonTool.Editor
    {
        public class JsonToolAutoBuild : IPreprocessBuildWithReport
        {
            public int callbackOrder { get; }

            public void OnPreprocessBuild(BuildReport report)
            {
                if (JsonToolConfig.Config.AutoCreateOnBuild)
                {
                    JsonToolConfigEditor.SaveJson();
                }
            }
        }
    }
}