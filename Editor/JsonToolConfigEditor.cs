#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
using System.Windows.Forms;
#endif

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace DailyCode.JsonTool
{
    [CustomEditor(typeof(JsonToolConfig))]
    public class JsonToolConfigEditor : Editor
    {
        private static List<string> outputName = new List<string>();
        public static JsonToolConfig _config;

        [UnityEditor.MenuItem("DuskLight/JsonTool/設定", false, priority: 0)]
        public static void GetOrCreateConfig()
        {
            JsonToolConfig.LoadOrCreateConfig();
        }

        public override void OnInspectorGUI()
        {
            _config = (JsonToolConfig)target;

            base.OnInspectorGUI();
            if (GUILayout.Button("導出 Json"))
            {
                ExportFileWin();
                ExportFileOSX();
                if(!string.IsNullOrEmpty(_config.SaveFilePath))
                    EditorUtility.RevealInFinder(_config.SaveFilePath);
            }
        }

        public void ExportFileOSX()
        {
#if UNITY_STANDALONE_OSX

#endif
        }
        public void ExportFileWin()
        {
#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
            var folderBrowser = new SaveFileDialog();
            folderBrowser.Filter = "Json files (*.json)|*.json";
            folderBrowser.FilterIndex = 2;
            folderBrowser.RestoreDirectory = true;
            
            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                var folderPath = Path.GetDirectoryName(folderBrowser.FileName);
                _config.SaveFilePath = folderPath;
                foreach (var item in _config.DataList)
                {
                    OutputFile(item, folderPath);
                }
            
                var result = new StringBuilder();
                for (int i = 0; i < outputName.Count; i++)
                {
                    result.Append(outputName[i]);
                    result.Append("\n");
                }
                result.Append($"共導出 {outputName.Count} 個檔案");
            
                EditorUtility.DisplayDialog("導出結果", result.ToString(), "確認");
            }
#endif
        }

        public static void SaveJson()
        {
            outputName.Clear();

            if (!string.IsNullOrEmpty(_config.SaveFilePath))
            {
                foreach (var item in _config.DataList)
                {
                    OutputFile(item, _config.SaveFilePath);
                }
                EditorUtility.RevealInFinder(_config.SaveFilePath);
            }
            else
            {
                Debug.LogWarning("JsonTool 未設置倒出路徑，請手動導出。");
            }
        }
        private static void OutputFile(ScriptableObject item, string path)
        {
            if (item != null)
            {
                outputName.Add(item.name);
                path = $"{path}/{item.name}.json";
                var s = JsonUtility.ToJson(item, false);

                using var sw = new StreamWriter(path);
                sw.Write(s);
                sw.Close();
            }
        }
    }
}
